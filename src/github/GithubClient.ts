import IGithubClient from "./IGithubClient";
import IGithubProject from "./IGithubProject";
import { IGithubError, GithubErrorType } from "./IGithubError";
import IGithubCommit from "./IGithubCommit";
import { Octokit } from "@octokit/rest"
import IGithubConfig from "./IGithubConfig";
import parse from "parse-link-header";
import IGithubPagedCommits from "./IGithubPagedCommits";

/**
 * Custom Github Error type.
 * @todo: refactor, instead of 'type' create separate class for each error.
 */
class GithubError extends Error implements IGithubError {
  type: GithubErrorType

  constructor(type?: GithubErrorType) {
    super("Github error");
    this.type = typeof(type) == 'number' ? type : GithubErrorType.UNSPECIFIED_ERROR;
  }
}

/**
 * Github client wrapper. Implementation uses ocktokit and
 * transforms github responses into internal application
 * interfaces.
 */
class GithubClient implements IGithubClient {

  octokit: Octokit

  constructor(octokit: Octokit) {
    this.octokit = octokit;
  }

  /**
   * Function loads ALL projects / repositories for provided
   * github organization.
   *
   * @param org
   */
  async loadAllProjects(org: string): Promise<IGithubProject[]>{
    let results: IGithubProject[] = [];
    try {
      for await (const response of this.octokit.paginate.iterator(
        "GET /orgs/:org/repos",
        { org: org, per_page: 100 }
      )) {
        let status = response.status;
        let data = response.data as any;
        if (status === 200) {
          for (let entry of data) {
            results.push({
              id: entry.id,
              name: entry.name,
              description: entry.description,
              avatarUrl: entry.owner.avatar_url,
              htmlUrl: entry.html_url,
              forks: entry.forks_count,
              issues: entry.open_issues_count,
              watchers: entry.watchers_count,
              language: entry.language || "",
            })
          }
        } else {
          throw new GithubError();
        }
      }
      return results;
    } catch (e) {
      if (e.code === 404) {
        throw new GithubError(GithubErrorType.ORGANIZATION_DOES_NOT_EXIST);
      }
      throw new GithubError();
    }
  }

  /**
   * Function loads paged commits from github.
   *
   * @param org organization name
   * @param project project/repository name
   * @param page
   * @param perPage
   */
  async loadPagedCommits(org: string, project: string,
                         page: number = 1, perPage: number = 30): Promise<IGithubPagedCommits> {
    let results: IGithubCommit[] = [];
    try {
      let { status, data, headers } = await this.octokit.repos.listCommits( {
        owner: org,
        repo: project,
        page: page,
        perPage: perPage
      });

      if (status === 200) {
        for (let entry of data) {
          results.push({
            sha: entry.sha,
            date: new Date(entry.commit.author.date),
            message: entry.commit.message,
            url: entry.html_url,
            author: {
              name: entry.commit.author.name,
              avatarUrl: entry.author? entry.author.avatar_url : "",
            }
          });
        }

        let prasedLinkHeader = parse(headers.link || "");

        return {
          commits: results,
          page: page,
          perPage: perPage,
          totalPages: prasedLinkHeader ? +prasedLinkHeader.last.page : 1
        }

      } else {
        throw new GithubError();
      }

    } catch (e) {
      throw new GithubError();
    }
  }
}

/**
 * Factory method to create github client. Can override ocktokit for testing purposes.
 *
 * @param config
 * @param octokit
 */
export default function createClient(config: IGithubConfig, octokit?: any): IGithubClient {
  if (octokit) {
    return new GithubClient(octokit);
  } else {
    octokit = new Octokit({
      auth: config.authToken || "",
      baseUrl: config.apiEndpoint || ""
    });
    return new GithubClient(octokit);
  }
}


