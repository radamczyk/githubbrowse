import IGithubProject from "./IGithubProject";
import IGithubCommit from "./IGithubCommit";
import IGithubPagedCommits from "./IGithubPagedCommits";

export default interface IGithubClient {
  loadAllProjects(org: String): Promise<IGithubProject[]>
  loadPagedCommits(org: String, project: String, page?: number): Promise<IGithubPagedCommits>
}

