
export default interface IGithubProject {
  id: number,
  name: string
  description: string
  avatarUrl: string
  htmlUrl: string
  forks: number
  issues: number
  watchers: number
  language: string
};

