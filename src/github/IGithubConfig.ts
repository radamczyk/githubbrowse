export default interface IGithubConfig {
  authToken: String
  apiEndpoint: String
}