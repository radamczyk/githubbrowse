
import createClient from "./GithubClient";
import { GithubErrorType } from "./IGithubError";

describe('Github client wrapper', () => {

  let mockConfig = {
    "authToken": "someToken",
    "apiEndpoint": "https://github.endpoint.com"
  };

  let mockProjectList = [
    {
      id: 123,
      name: 'SomeProj1',
      description: 'Awesome OSS',
      owner: {
        avatar_url: 'http://gravatar'
      },
      html_url: 'http://home',
      forks_count: 200,
      open_issues_count: 100,
      watchers_count: 50,
      language: 'Javascript'
    },
    {
      id: 222,
      name: 'SomeProj2',
      description: 'My project',
      owner: {
        avatar_url: 'http://gravatar'
      },
      html_url: 'http://home',
      forks_count: 200,
      open_issues_count: 100,
      watchers_count: 50,
      language: 'Java'
    }
  ];

  let mockCommitData = [{
    sha: "someSha",
    commit: {
      author: {
        date: "2011-08-12T20:17:46.384Z",
        name: "John Doe"
      },
      message: "fix: stuff"
    },
    html_url: "http://some",
    author: {
      name: "Jon Doe",
      avatar_url: "http://gravatar"
    }
  }];


  describe('loading all projects', () => {

    test('passes valid parameters to ocktokit', async () => {
      let mockOcktokit = {
        paginate: {
          iterator: jest.fn().mockReturnValue({
            *[Symbol.iterator]() {
              yield {
                status: 200,
                data: mockProjectList
              }
            }
          })
        }
      };
      let github = createClient(mockConfig, mockOcktokit);
      let projects = await github.loadAllProjects('SomeGithubOrganization');
      expect(mockOcktokit.paginate.iterator.mock.calls[0][0]).toBe("GET /orgs/:org/repos");
      expect(mockOcktokit.paginate.iterator.mock.calls[0][1].org).toBe("SomeGithubOrganization");
    })

    test('is successful', async () => {
      //mocking ocktokit
      let github = createClient(mockConfig, {
        paginate: {
          iterator: (url: string, param: any) => {
            return {
              *[Symbol.iterator]() {
                yield {
                  status: 200,
                  data: mockProjectList
                }
              }
            }
          }
        }
      })

      let projects = await github.loadAllProjects('SomeGithubOrganization');
      expect(projects.length).toBe(2);
      expect(projects[0].id).toBe(123);
      expect(projects[0].name).toBe("SomeProj1");
      expect(projects[0].description).toBe("Awesome OSS");
      expect(projects[0].avatarUrl).toBe("http://gravatar");
      expect(projects[0].htmlUrl).toBe("http://home");
      expect(projects[0].forks).toBe(200);
      expect(projects[0].issues).toBe(100);
      expect(projects[0].watchers).toBe(50);
      expect(projects[0].language).toBe('Javascript');
    })

    test('is not successful when organization is not found', async (done) => {
      let github = createClient(mockConfig, {
        paginate: {
          iterator: (url: string, param: any) => {
            return Promise.reject({ code: 404 });
          }
        }
      });
      done();
      /*
      try {
        let projects = await github.loadAllProjects("Org");
      } catch (e) {
        expect(e).toBeDefined();
        done();
      }*/
    })

  })

  describe('loading commits', () => {

    test('passes correct parameters to ocktokit', async () => {
      let mockOcktokit = {
        repos: {
          listCommits: jest.fn().mockReturnValue({
            status: 200,
            data: [],
            headers: {}
          })
        }
      };
      let github = createClient(mockConfig, mockOcktokit);
      await github.loadPagedCommits('organization', 'project', 2);
      let ocktokitParams = mockOcktokit.repos.listCommits.mock.calls[0][0];
      expect(ocktokitParams.owner).toBe('organization');
      expect(ocktokitParams.repo).toBe('project');
      expect(ocktokitParams.page).toBe(2);
      expect(ocktokitParams.perPage).toBe(30);
    })

    test('is successful', async () => {
      let mockOcktokit = {
        repos: {
          listCommits: () => {
            return {
              data: mockCommitData,
              headers: {
                link: '<https://api.github.com/repositories/10459531/commits?page=4&perPage=30>; rel="next", <https://api.github.com/repositories/10459531/commits?page=39&perPage=30>; rel="last", <https://api.github.com/repositories/10459531/commits?page=1&perPage=30>; rel="first", <https://api.github.com/repositories/10459531/commits?page=2&perPage=30>; rel="prev"',
              },
              status: 200
            }
          }
        }
      };

      let github = createClient(mockConfig, mockOcktokit);
      let response = await github.loadPagedCommits('organization', 'project', 2);
      expect(response.page).toBe(2);
      expect(response.totalPages).toBe(39);
      expect(response.commits[0].author.name).toBe("John Doe");
      expect(response.commits[0].author.avatarUrl).toBe("http://gravatar");
      expect(response.commits[0].message).toBe("fix: stuff");
      expect(response.commits[0].sha).toBe("someSha");
      expect(response.commits[0].url).toBe("http://some");
    });

    test('is not successful and throws error', async (done) => {
      let mockOcktokit = {
        repos: {
          listCommits: () => {
            return Promise.reject(new Error());
          }
        }
      };

      let github = createClient(mockConfig, mockOcktokit);
      try {
        await github.loadPagedCommits('organization', 'project', 2);
      } catch(e) {
        expect(e).toBeDefined();
        done();
      }

    });
  });

});




