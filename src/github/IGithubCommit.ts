
export interface ICommitAuthor {
  name: string
  avatarUrl: string
}

export default interface IGithubCommit {
  author: ICommitAuthor
  date: Date
  url: string
  message: string
  sha: string
}

