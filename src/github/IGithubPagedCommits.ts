import IGithubCommit from "./IGithubCommit";

export default interface IGithubPagedCommits {
  commits: IGithubCommit[]
  page: number
  perPage: number
  totalPages: number
}


