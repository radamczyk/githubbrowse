
export enum GithubErrorType {
  ORGANIZATION_DOES_NOT_EXIST,
  CONNECTION_ISSUE,
  UNSPECIFIED_ERROR
}

export interface IGithubError {
  type: GithubErrorType
}


