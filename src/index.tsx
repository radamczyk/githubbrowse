import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App/App';
import * as serviceWorker from './serviceWorker';
import createGithubClient from './github/GithubClient';
import config from './Config';
import createStore from './stores/ApplicationStore';

let githubClient = createGithubClient(config.github);
let appStore = createStore(githubClient);

ReactDOM.render(
  <React.StrictMode>
    <App store={appStore}/>
  </React.StrictMode>,
  document.getElementById('root')
);

serviceWorker.unregister();
