
import createStore, { ApplicationStore } from './ApplicationStore';
import IGithubClient from '../github/IGithubClient';
import { ActionStatus } from './IApplicationStore';
import IGithubProject from '../github/IGithubProject';
import IGithubCommit from '../github/IGithubCommit';

describe('Application Store', () => {

  let projectsMockData: IGithubProject[];
  let commitsMockData: IGithubCommit[];
  let githubMock: IGithubClient;

  beforeEach(() => {
    projectsMockData = [{
      id: 123,
      name: "Proj1",
      description: "Description 1",
      avatarUrl: "http://gravatar1",
      htmlUrl: "http://url",
      forks: 20,
      issues: 30,
      watchers: 10,
      language: "Javascript"
    }];

    commitsMockData = [{
      author: {
        name: "John Doe",
        avatarUrl: "http://gravatar/user"
      },
      date: new Date(),
      url: "http://github",
      message: "fix:bugs",
      sha: "123abdc"
    }];

    githubMock = {
      loadAllProjects: jest.fn().mockReturnValue(projectsMockData),
      loadPagedCommits: jest.fn().mockReturnValue({
        commits: commitsMockData,
        page: 1,
        perPage: 10,
        totalPages: 100
      })
    };
  })

  describe('clearing status', () => {

    test('clears action status and error', () => {
      let appStore = createStore(githubMock);
      appStore.actionStatus = ActionStatus.PENDING;
      appStore.error = { type: 0 };
      appStore.clearStatus();
      expect(appStore.actionStatus).toBe(ActionStatus.COMPLETED);
      expect(appStore.error).toBeUndefined();
    });

  });

  describe('loading github projects', () => {
    let appStore: ApplicationStore;

    beforeAll(() => {
      appStore = createStore(githubMock);
      appStore.commitsStore.reset = jest.fn();
      appStore.projectStore.loadGithubProjects = jest.fn();
      appStore.loadGithubProjects("MyOrganization");
    });

    test('sets the organization name', () => {
      expect(appStore.organization).toBe("MyOrganization");
    })

    test('resets commits store', () => {
      expect(appStore.commitsStore.reset).toBeCalled();
      expect(appStore.commitsStore.page).toBe(1);
    });

    test('uses project store to load repositories', () => {
      expect(appStore.projectStore.loadGithubProjects).toBeCalled();
    });

  })

   describe('changes statuses during loading', () => {

    test('to pending and complete after loading projects', (done) => {
      githubMock.loadAllProjects = async (org) => {
        return Promise.resolve(projectsMockData);
      };
      let appStore = createStore(githubMock);
      appStore.loadGithubProjects('organization').then(() => {
        expect(appStore.actionStatus).toBe(ActionStatus.COMPLETED);
        done();
      });
      expect(appStore.actionStatus).toBe(ActionStatus.PENDING);
    })

    test('to pending and complete after loading commits', (done) => {
      githubMock.loadPagedCommits = async (org, project, page) => {
        return Promise.resolve({
          commits: commitsMockData,
          page: 1,
          perPage: 10,
          totalPages: 100
        });
      };
      let appStore = createStore(githubMock);
      appStore.loadCommits('organization').then(() => {
        expect(appStore.actionStatus).toBe(ActionStatus.COMPLETED);
        done();
      });
      expect(appStore.actionStatus).toBe(ActionStatus.PENDING);
    })

   })

});
