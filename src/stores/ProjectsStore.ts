import { observable, computed, action } from 'mobx';
import IGithubProject from '../github/IGithubProject';
import { ActionStatus, ProjectOrder, IProjectStore } from './IApplicationStore';
import { ApplicationStore } from './ApplicationStore';
import IGithubClient from '../github/IGithubClient';

/**
 * Project store is responsible for holding state for the project list related components
 * It uses github client wrapper to load all projects for chosen organization.
 */
export default class ProjectsStore implements IProjectStore {
  //right not arbitrary not configurable (todo: fix to be in config)
  @observable
  perPage: number = 10

  @observable
  page: number = 1

  @observable
  totalPages: number = 0

  @observable
  orderBy: ProjectOrder = ProjectOrder.NUMBER_OF_FORKS

  @observable
  organization: string = ""

  @observable
  projects: IGithubProject[] = []

  @observable
  loadingStatus: ActionStatus = ActionStatus.COMPLETED

  appStore: ApplicationStore

  github: IGithubClient

  constructor(appStore: ApplicationStore, github: IGithubClient) {
    this.appStore = appStore;
    this.github = github;
  }

  @action
  async loadGithubProjects(organizationName: string) {
    this.projects = [];
    this.organization = organizationName;
    this.loadingStatus = ActionStatus.PENDING;
    try {
      this.projects = await this.github.loadAllProjects(organizationName);
      this.totalPages = Math.ceil(this.projects.length / this.perPage);
      this.loadingStatus = ActionStatus.COMPLETED;
    } catch(e) {
      this.loadingStatus = ActionStatus.ERROR;
      this.appStore.error = e;
    }
  }

  @action
  setPage(page: number) {
    if (page < 1) {
      this.page = 1;
    } else if (page > this.totalPages) {
      this.page = this.totalPages;
    } else {
      this.page = page;
    }
  }

  @computed
  get orderedProjects(): IGithubProject[] {
    let sortedProjects = this.projects.slice(0);
    sortedProjects.sort((projectA, projectB) => {
      return projectB.forks - projectA.forks;
    })
    let startIdx = (this.page - 1) * this.perPage,
        endIdx = startIdx + this.perPage;
    return sortedProjects.slice(startIdx, endIdx);
  }

}
