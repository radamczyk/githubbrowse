import IGithubCommit from "../github/IGithubCommit";
import { ApplicationStore } from "./ApplicationStore";
import CommitsStore from "./CommitsStore";
import { ActionStatus } from "./IApplicationStore";
import { GithubErrorType } from "../github/IGithubError";
jest.mock("./ApplicationStore");

describe('Commits store', () => {
  let commitsMockData: IGithubCommit[];
  let githubMock: any;
  let appStoreMock: ApplicationStore;

  beforeEach(() => {
    commitsMockData = [{
      author: {
        name: "John Doe",
        avatarUrl: "http://gravatar/user"
      },
      date: new Date(),
      url: "http://github",
      message: "fix:bugs",
      sha: "123abdc"
    }];

    githubMock = {
      loadAllProjects: jest.fn(),
      loadPagedCommits: jest.fn().mockReturnValue(Promise.resolve({
        commits: commitsMockData,
        page: 2,
        perPage: 10,
        totalPages: 100
      }))
    };
    appStoreMock = new ApplicationStore(githubMock);
  });

  describe('when loading commits', () => {

    let commitStore: CommitsStore;

    beforeEach(() => {
      commitStore = new CommitsStore(appStoreMock, githubMock);
    });

    test('clears existing commits', () => {
      commitStore.commits = commitsMockData;
      commitStore.loadCommits('gitorg', 'project');
      expect(commitStore.commits.length).toBe(0);
    });

    test('sets organization and project', () => {
      commitStore.loadCommits('gitorg', 'project');
      expect(commitStore.organization).toBe('gitorg');
      expect(commitStore.project).toBe('project');
    })

    test('sets status to pending', () => {
      commitStore.loadCommits('gitorg', 'project');
      expect(commitStore.loadingStatus).toBe(ActionStatus.PENDING);
    })

    test('assigns response data and sets paging values', async () => {
      await commitStore.loadCommits('gitorg', 'project');
      expect(commitStore.commits.length).toBe(1);
      expect(commitStore.commits[0].url).toBe('http://github');
      expect(commitStore.totalPages).toBe(99);
      expect(commitStore.loadingStatus).toBe(ActionStatus.COMPLETED);
    })

    test('propagates error to parent store', async () => {
      githubMock.loadPagedCommits = jest.fn().mockReturnValue(Promise.reject({
        type: GithubErrorType.UNSPECIFIED_ERROR
      }));
      commitStore = new CommitsStore(appStoreMock, githubMock);
      await commitStore.loadCommits('gitorg', 'project');
      expect(appStoreMock.error?.type).toBe(GithubErrorType.UNSPECIFIED_ERROR);
      expect(commitStore.loadingStatus).toBe(ActionStatus.ERROR);
    })

  });

  describe('when page set action is triggered', ()=> {
    let commitStore: CommitsStore;

    beforeEach(() => {
      commitStore = new CommitsStore(appStoreMock, githubMock);
    });

    test ('it uses the page to load commits', async ()=> {
      await commitStore.loadCommits('gitorg', 'project');
      await commitStore.setPage(5);
      let pageCallGitArgs = githubMock.loadPagedCommits.mock.calls[1];
      expect(pageCallGitArgs[0]).toBe('gitorg');
      expect(pageCallGitArgs[1]).toBe('project');
      expect(pageCallGitArgs[2]).toBe(5);
    })

    test('it prevents setting page lower then 1', async () => {
      await commitStore.setPage(0);
      expect(commitStore.page).toBe(1);
    })

    test('it prevent setting page greater than current total number of pages', async () => {
      commitStore.totalPages = 100;
      await commitStore.setPage(120);
      expect(commitStore.page).toBe(100);
    });

  });

  describe('when reset action is triggered', () => {
    test('sets values to defaults', () => {
      let commitStore = new CommitsStore(appStoreMock, githubMock);
      commitStore.reset();
      expect(commitStore.page).toBe(1);
      expect(commitStore.totalPages).toBe(0);
      expect(commitStore.organization).toBe("");
      expect(commitStore.project).toBe("");
    });
  })

});