import { ApplicationStore } from "./ApplicationStore";
import IGithubCommit from "../github/IGithubCommit";
import IGithubProject from "../github/IGithubProject";
import ProjectsStore from "./ProjectsStore";
import { ActionStatus } from "./IApplicationStore";
import { GithubErrorType } from "../github/IGithubError";
import { exec } from "child_process";

describe('Commits store', () => {
  let projectsMockData: IGithubProject[];
  let githubMock: any;
  let appStoreMock: ApplicationStore;

  beforeEach(() => {
    projectsMockData = [{
      id: 123,
      name: "Proj1",
      description: "Description 1",
      avatarUrl: "http://gravatar1",
      htmlUrl: "http://url",
      forks: 1,
      issues: 30,
      watchers: 10,
      language: "Javascript"
    },
    {
      id: 124,
      name: "Proj2",
      description: "Description 2",
      avatarUrl: "http://gravatar2",
      htmlUrl: "http://url2",
      forks: 20,
      issues: 30,
      watchers: 10,
      language: "Javascript2"
    }];

    githubMock = {
      loadAllProjects: jest.fn().mockReturnValue(Promise.resolve(projectsMockData)),
      loadPagedCommits: jest.fn()
    };
    appStoreMock = new ApplicationStore(githubMock);
  });

  describe('loading github projects', () => {
    let projectStore: ProjectsStore;

    beforeEach(() => {
      projectStore = new ProjectsStore(appStoreMock, githubMock);
    })

    test('resets existing projects, organization and changes status', () => {
      projectStore.projects = [
        {
          id: 123,
          name: "ToBeCleared",
          description: "ToBeCleared ",
          avatarUrl: "ToBeCleared",
          htmlUrl: "ToBeCleared",
          forks: 20,
          issues: 30,
          watchers: 10,
          language: "ToBeCleared"
        }
      ];
      projectStore.loadGithubProjects('neworganization');
      expect(projectStore.projects.length).toBe(0);
      expect(projectStore.organization).toBe('neworganization');
      expect(projectStore.loadingStatus).toBe(ActionStatus.PENDING);
    });

    test('loads projects and calculates total pages', async () => {
      let mockList = [];
      for (let i = 0; i < 20; i++) {
        mockList.push({
          id: i,
          name: "Proj1",
          description: "Description 1",
          avatarUrl: "http://gravatar1",
          htmlUrl: "http://url",
          forks: i,
          issues: 30,
          watchers: 10,
          language: "Javascript"
        })
      }
      githubMock.loadAllProjects = jest.fn().mockReturnValue(Promise.resolve(mockList));
      projectStore = new ProjectsStore(appStoreMock, githubMock);
      await projectStore.loadGithubProjects('githuborg');
      expect(projectStore.totalPages).toBe(2);
      expect(projectStore.projects.length).toBe(20);
      expect(projectStore.loadingStatus).toBe(ActionStatus.COMPLETED);
    });

    test('sets application error when something wrong happens on the github client end', async () => {
      githubMock.loadAllProjects = jest.fn().mockReturnValue(Promise.reject({
        type: GithubErrorType.ORGANIZATION_DOES_NOT_EXIST
      }));
      projectStore = new ProjectsStore(appStoreMock, githubMock);
      await projectStore.loadGithubProjects('organization');
      expect(projectStore.loadingStatus).toBe(ActionStatus.ERROR);
      expect(appStoreMock.error?.type).toBe(GithubErrorType.ORGANIZATION_DOES_NOT_EXIST);
    })

  });

  describe('when accessing ordered project computed store value', () => {
    let projectStore: ProjectsStore;

    beforeEach(() => {
      let mockList = [];
      for (let i = 0; i < 20; i++) {
        mockList.push({
          id: i + 1,
          name: "Proj1",
          description: "Description 1",
          avatarUrl: "http://gravatar1",
          htmlUrl: "http://url",
          forks: i + 1,
          issues: 30,
          watchers: 10,
          language: "Javascript"
        })
        githubMock.loadAllProjects = jest.fn().mockReturnValue(Promise.resolve(mockList));
        projectStore = new ProjectsStore(appStoreMock, githubMock);
      }
    });


    test('projects are returned in number of forks order', async () => {
      await projectStore.loadGithubProjects('githuborg');
      let orderedProjects = projectStore.orderedProjects;
      expect(orderedProjects[0].forks).toBe(20);
      expect(orderedProjects[1].forks).toBe(19);
    });

    test('it will returned sorted page when set page was called', async() => {
      await projectStore.loadGithubProjects('githuborg');
      let orderedProjects = projectStore.orderedProjects;
      expect(orderedProjects[0].forks).toBe(20);
      projectStore.setPage(2);
      orderedProjects = projectStore.orderedProjects;
      expect(orderedProjects[0].forks).toBe(10);
    });

  });

});