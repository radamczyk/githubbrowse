import { IGithubError } from "../github/IGithubError"
import IGithubCommit from "../github/IGithubCommit"
import IGithubProject from "../github/IGithubProject"

/**
 * This module defines a set of store interfaces used by the
 * react presentation layer of the application. Interfaces define
 * properties used to display in UI and actions which are triggered
 * from the user interface.
 */

export enum ActionStatus {
  PENDING,
  COMPLETED,
  ERROR
}

export enum ProjectOrder {
  NUMBER_OF_FORKS
}

export interface IPaginatedStore {
  page: number;
  totalPages: number;
  perPage: number;
  setPage(page: number): void
}

export interface ICommitsStore extends IPaginatedStore{
  commits: IGithubCommit[]
  project: string
  organization: string
  loadingStatus: ActionStatus
}

export interface IProjectStore extends IPaginatedStore{
  orderBy: ProjectOrder
  organization: string
  loadingStatus: ActionStatus
  orderedProjects: IGithubProject[]
}

export interface IApplicationStore {
  organization: string
  error?: IGithubError
  actionStatus: ActionStatus
  commitsStore: ICommitsStore
  projectStore: IProjectStore

  loadGithubProjects(organizationName: string): void
  loadCommits(project: string): void
  clearStatus(): void
}
