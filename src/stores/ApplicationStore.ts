import { observable, action } from 'mobx';
import { IGithubError } from '../github/IGithubError';
import IGithubClient from '../github/IGithubClient';
import { IApplicationStore, ActionStatus } from './IApplicationStore';
import ProjectsStore  from './ProjectsStore';
import CommitsStore from './CommitsStore';

/**
 * Central observable store for the application UI.
 * handles error main loader status and propagates
 * actions to sub-stores. Application store is the
 * main interface for the UI view components.
 */
export class ApplicationStore implements IApplicationStore {

  github: IGithubClient

  @observable
  organization: string = ""

  @observable
  error?: IGithubError

  @observable
  actionStatus: ActionStatus = ActionStatus.COMPLETED

  commitsStore: CommitsStore

  projectStore: ProjectsStore

  constructor(github: IGithubClient) {
    this.github = github;
    this.commitsStore = new CommitsStore(this, github);
    this.projectStore = new ProjectsStore(this, github);
  }

  @action
  clearStatus() {
    this.actionStatus = ActionStatus.COMPLETED;
    this.error = undefined;
  }

  @action
  async loadGithubProjects(organizationName: string) {
    this.commitsStore.reset();
    this.commitsStore.page = 1;
    this.organization = organizationName;
    this.actionStatus = ActionStatus.PENDING;
    await this.projectStore.loadGithubProjects(organizationName);
    this.actionStatus = ActionStatus.COMPLETED;
  }

  @action
  async loadCommits(project: string) {
    this.actionStatus = ActionStatus.PENDING;
    await this.commitsStore.loadCommits(this.organization, project);
    this.actionStatus = ActionStatus.COMPLETED;
  }
}

export default function createStore(githubClient: IGithubClient) {
  return new ApplicationStore(githubClient);
}


