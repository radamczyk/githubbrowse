import { observable, action } from 'mobx';
import IGithubCommit from '../github/IGithubCommit';
import { ActionStatus, ICommitsStore } from './IApplicationStore';
import { ApplicationStore } from './ApplicationStore';
import IGithubClient from '../github/IGithubClient';

/**
 * Commits store is responsible for handling commits data and state only.
 */
export default class CommitsStore implements ICommitsStore {

  @observable
  commits: IGithubCommit[] = []

  @observable
  page: number = 1

  perPage: number = 30

  @observable
  totalPages: number = 0

  @observable
  project: string = ""

  @observable
  organization: string = ""

  @observable
  loadingStatus: ActionStatus = ActionStatus.COMPLETED

  github: IGithubClient

  @action
  async loadCommits(organization: string, project: string) {
    this.commits = [];
    this.project = project;
    this.organization = organization;
    this.loadingStatus = ActionStatus.PENDING;
    try {
      let commitsPaged = await this.github.loadPagedCommits(organization, project, this.page);
      this.commits = commitsPaged.commits;
      this.totalPages = commitsPaged.totalPages - 1;
      this.loadingStatus = ActionStatus.COMPLETED;
    } catch (e) {
      this.loadingStatus = ActionStatus.ERROR;
      this.appStore.error = e;
    }
  }

  @action
  async setPage(page:number) {
    if (page < 1) {
      this.page = 1;
    } else if (page > this.totalPages) {
      this.page = this.totalPages;
    } else {
      this.page = page;
    }
    this.loadCommits(this.organization, this.project);
  }

  reset() {
    this.commits = [];
    this.page = 1;
    this.totalPages = 0;
    this.organization = "";
  }

  appStore: ApplicationStore

  constructor(appStore: ApplicationStore, githubClient: IGithubClient) {
    this.appStore = appStore;
    this.github = githubClient;
  }

}
