import appConfig from './config.json';
import IGithubConfig from './github/IGithubConfig';


export interface IConfig {
  github: IGithubConfig
}

export default appConfig;

