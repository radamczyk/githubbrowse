import React from 'react';
import { render, fireEvent, screen} from '@testing-library/react';
import createMockApplicationStore from '../testhelpers/mockappstore';
import { IApplicationStore, ActionStatus, IPaginatedStore } from '../../stores/IApplicationStore';
import IGithubClient from '../../github/IGithubClient';
import ReactDOM from 'react-dom';
import { act } from 'react-dom/test-utils';
import Paginate from './Paginate';

describe('Paginate component', () => {

  let paginatedStoreMock: IPaginatedStore,
      container: HTMLElement;

  beforeEach(() => {
    paginatedStoreMock = {
      page: 1,
      totalPages: 10,
      perPage: 5,
      setPage: jest.fn()
    }
    container = document.createElement('div');
    document.body.appendChild(container);
  })

  afterEach(() => {
    document.body.removeChild(container);
  });

  test('shows up when total pages greater than 1', () => {
    const { getByRole } = render(<Paginate paginatedStore={paginatedStoreMock}/>);
    expect(getByRole('prev-page')).toBeInTheDocument();
    expect(getByRole('next-page')).toBeInTheDocument();
  })

  test('does not show up when there is only single page', () => {
    paginatedStoreMock.totalPages = 1;
    const { queryByRole } = render(<Paginate paginatedStore={paginatedStoreMock}/>);
    expect(queryByRole('prev-page')).toBeNull();
    expect(queryByRole('next-page')).toBeNull();
  })

  test('sets next page in store when next clicked', () => {
    paginatedStoreMock.page = 1;
    const { getByRole } = render(<Paginate paginatedStore={paginatedStoreMock}/>);
    fireEvent.click(getByRole('next-page'));
    expect(paginatedStoreMock.setPage).toBeCalledWith(2);
  })

  test('sets prev page in store when prev clicked', () => {
    paginatedStoreMock.page = 2;
    const { getByRole } = render(<Paginate paginatedStore={paginatedStoreMock}/>);
    fireEvent.click(getByRole('prev-page'));
    expect(paginatedStoreMock.setPage).toBeCalledWith(1);
  })

});