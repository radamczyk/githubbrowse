import React from 'react';
import { IPaginatedStore } from '../../stores/IApplicationStore';
import { Pagination } from 'react-bootstrap';
import { observer } from 'mobx-react';
import './Paginate.css'


type PaginateProps = {
  paginatedStore: IPaginatedStore
}


function Paginate(props: PaginateProps) {
  let store = props.paginatedStore,
      isPrevDisabled = store.page === 1,
      isNextDisabled = store.page === store.totalPages;

  function prevPage() {
    store.setPage(store.page - 1);
  }

  function nextPage() {
    store.setPage(store.page + 1);
  }

  if (store.totalPages > 1) {
    return (
      <Pagination size="sm" className="Paginate">
          <Pagination.Prev onClick={prevPage} role="prev-page" disabled={isPrevDisabled}/>

          <Pagination.Next onClick={nextPage} role="next-page" disabled={isNextDisabled}/>
      </Pagination>
    )
  }

  return (
    <>
    </>
  );
}

export default observer(Paginate);
