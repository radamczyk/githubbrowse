import React from 'react';
import { render } from '@testing-library/react';
import Commit from './Commit';
import IGithubCommit from '../../github/IGithubCommit';

describe('Commit component', () => {

  let commit: IGithubCommit;

  beforeEach(() => {
    commit = {
      author: {
        name: "John Doe",
        avatarUrl: "http://gravatar/user"
      },
      date: new Date("2020-05-04 5:00:00 PM"), //5/3/2020, 5:00:00 PM
      url: "http://github",
      message: "fix:bugs",
      sha: "1234567890"
    }

  });

  test('displays only 6 first sha characters', () => {
    let { getByText } = render(<Commit commit={commit}/>);
    expect(getByText('123456')).toBeInTheDocument();
  })

  test('displays author name', () => {
    let { getByText } = render(<Commit commit={commit}/>);
    expect(getByText('John Doe')).toBeInTheDocument();
  })

  test('displays commit message', () => {
    let { getByText } = render(<Commit commit={commit}/>);
    expect(getByText('fix:bugs')).toBeInTheDocument();
  })

  test('displays commit date', () => {
    let { getByText } = render(<Commit commit={commit}/>);
    expect(getByText('5/4/2020, 5:00:00 PM')).toBeInTheDocument();
  })

});
