import React from 'react';
import { render } from '@testing-library/react';
import CommitList from './CommitList';
import createMockApplicationStore from '../testhelpers/mockappstore';
import { IApplicationStore } from '../../stores/IApplicationStore';
import IGithubClient from '../../github/IGithubClient';

describe('Commit list', () => {

  let appStoreMock: IApplicationStore,
      githubMock: IGithubClient,
      container: HTMLElement;

  beforeEach(() => {
    let mocks = createMockApplicationStore();
    appStoreMock = mocks.appStoreMock;
    githubMock = mocks.githubMock;
  })

  test('renders project name', () => {
    appStoreMock.commitsStore.project = "myrepo";
    let { getByText } = render(<CommitList store={appStoreMock}/>);
    expect(getByText('myrepo')).toBeInTheDocument();
  });

  test('renders all commits', () => {
    appStoreMock.commitsStore.commits = [{
      author: {
        name: "John Doe",
        avatarUrl: "http://gravatar/user"
      },
      date: new Date(),
      url: "http://github",
      message: "fix:bugs",
      sha: "1234567",
    },
    {
      author: {
        name: "John Not Doe",
        avatarUrl: "http://gravatar/user"
      },
      date: new Date(),
      url: "http://github",
      message: "fix:bugs",
      sha: "123abdc"
    }];
    let { getByText } = render(<CommitList store={appStoreMock}/>);
    expect(getByText('John Doe')).toBeInTheDocument();
    expect(getByText('John Not Doe')).toBeInTheDocument();
  });

});