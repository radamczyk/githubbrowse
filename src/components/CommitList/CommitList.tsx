import React from 'react';
import { observer } from 'mobx-react';
import { ListGroup, Col } from 'react-bootstrap';
import Commit from './Commit';
import { IApplicationStore, ICommitsStore } from '../../stores/IApplicationStore';
import Paginate from '../Paginate/Paginate';
import './CommitList.css';

type CommitListProps = {
  store: IApplicationStore
}

function CommitList(props: CommitListProps) {
  const commitStore: ICommitsStore = props.store.commitsStore;
  const commits = commitStore.commits.map((commit) =>
    <Commit commit={commit} key={commit.sha}/>
  );
  return (
    <Col className="CommitList AppList">
      <header>
        <h4>Commits</h4>
        <span>{commitStore.project}</span>
      </header>
      <Paginate paginatedStore={commitStore}/>
      <ListGroup as="ul">
        {commits}
      </ListGroup>
    </Col>
  )
}

export default observer(CommitList);