import React from 'react';
import IGithubCommit from '../../github/IGithubCommit';
import { ListGroup } from 'react-bootstrap';
import './Commit.css';

type CommitProps = {
  commit: IGithubCommit
}

export default function Commit(props: CommitProps) {

  const commit = props.commit;
  //quick fix to use empty image in case no avatar to avoid empty 'src' attribute
  const avatarImage = commit.author.avatarUrl || 'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=';

  return (
    <ListGroup.Item as="li" className="Commit">
      <span className="Commit-sha">{commit.sha.substring(0, 6)}</span>
      <span className="Commit-date">{commit.date.toLocaleString()}</span>
      <img className="Commit-avatar" src={avatarImage} height="20px" alt={commit.author.name}></img>
      <span className="Commit-author"><b>{commit.author.name}</b></span>
      <a href={commit.url} target="_blank" className="Commit-btn btn btn-outline-primary btn-sm" rel="noopener noreferrer">Github</a>
      <p className="Commit-message">{commit.message}</p>

    </ListGroup.Item>
  );
}