import React from 'react';
import { render, fireEvent, screen} from '@testing-library/react';
import App from './App';
import createMockApplicationStore from '../testhelpers/mockappstore';
import { IApplicationStore, ActionStatus } from '../../stores/IApplicationStore';
import IGithubClient from '../../github/IGithubClient';
import ReactDOM from 'react-dom';
import { act } from 'react-dom/test-utils';


describe('Main application component', () => {

  const INPUT_PLACEHOLDER = 'Enter github organization name';

  let appStoreMock: IApplicationStore,
      githubMock: IGithubClient,
      container: HTMLElement;

  beforeEach(() => {
    let mocks = createMockApplicationStore();
    appStoreMock = mocks.appStoreMock;
    githubMock = mocks.githubMock;
    container = document.createElement('div');
    document.body.appendChild(container);
  })

  afterEach(() => {
    document.body.removeChild(container);
  });

  test('renders search input nad project search button', () => {
    const { getByPlaceholderText, getByRole } = render(<App store={appStoreMock}/>);
    expect(getByPlaceholderText(INPUT_PLACEHOLDER)).toBeInTheDocument();
    expect(getByRole('button-main')).toBeInTheDocument();
  })

  test('renders loading button when application is loading projects', () => {
    appStoreMock.actionStatus = ActionStatus.PENDING;
    const { getByPlaceholderText, getByRole } = render(<App store={appStoreMock}/>);
    expect(getByRole('button-loading')).toBeInTheDocument();
  })

  test('loads projects when button is pressed', async () => {
    await act(async () => {
      ReactDOM.render(<App store={appStoreMock}/>, container);
    })
    let input = screen.getByPlaceholderText(INPUT_PLACEHOLDER);
    await act(async () => {
      fireEvent.change(input, { target: { value: 'githuborg' } })
      fireEvent.click(screen.getByRole('button-main'));
    })
    expect(githubMock.loadAllProjects).toBeCalled();
  })

  test('loads projects when enter is pressed on the input', async () => {
    await act(async () => {
      ReactDOM.render(<App store={appStoreMock}/>, container);
    })
    let input = screen.getByPlaceholderText(INPUT_PLACEHOLDER);
    await act(async () => {
      fireEvent.change(input, { target: { value: 'githuborg' } })
      fireEvent.keyDown(screen.getByPlaceholderText(INPUT_PLACEHOLDER), { keyCode: 13, target: input});
    })
    expect(githubMock.loadAllProjects).toBeCalled();
  })
})


