import React from 'react';
import { Container, Row, Form, Button, Spinner } from 'react-bootstrap';
import { observer } from 'mobx-react';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import ProjectList from '../ProjectList/ProjectList';
import CommitList from '../CommitList/CommitList';
import { IApplicationStore, ActionStatus } from '../../stores/IApplicationStore';
import ErrorAlert from '../ErrorAlert/ErrorAlert';

type AppProps = {
  store: IApplicationStore
}

function App(props: AppProps) {
  const appStore: IApplicationStore = props.store;
  let organizationName = "";

  function launchProjectsLoading() {
    if (organizationName !== "") {
      appStore.loadGithubProjects(organizationName);
    }
  }

  function onOrganizationInputChange(e: any) {
    organizationName = e.target.value;
  }

  function onOrganizationInputKewDown(e: any) {
    if (e.keyCode === 13 && e.target.value !== "") {
      e.preventDefault();
      launchProjectsLoading();
    } else {
      appStore.clearStatus();
    }
  }

  const isActionPending = appStore.actionStatus === ActionStatus.PENDING;
  let loadProjectsBtn;
  if (isActionPending) {
    loadProjectsBtn = (
      <Button variant="primary" role="button-loading" disabled>
        <Spinner as="span" animation="grow" size="sm" role="status" aria-hidden="true"/>
        Loading...
      </Button>
    );
  } else {
    loadProjectsBtn = (
      <Button variant="primary" role="button-main" onClick={launchProjectsLoading}>Load Projects</Button>
    );
  }

  return (
    <div className="App">
      <header className="App-header">
        <Form>
          <Form.Control type="text" placeholder="Enter github organization name"
              onKeyDown={onOrganizationInputKewDown} onChange={onOrganizationInputChange} disabled={isActionPending}/>
        </Form>
        {loadProjectsBtn}
      </header>
      <ErrorAlert store={appStore}/>
      <Container className="App-content" fluid>
        <Row>
          <ProjectList store={appStore}/>
          <CommitList store={appStore}/>
        </Row>
      </Container>
    </div>
  );
}

export default observer(App);
