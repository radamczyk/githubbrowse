import React from 'react';
import { render, fireEvent, screen} from '@testing-library/react';
import Project from './Project';
import createMockApplicationStore from '../testhelpers/mockappstore';
import { IApplicationStore } from '../../stores/IApplicationStore';
import IGithubClient from '../../github/IGithubClient';
import IGithubProject from '../../github/IGithubProject';

describe('Project component', () => {

  let appStoreMock: IApplicationStore,
      githubMock: IGithubClient,
      project: IGithubProject;

  beforeEach(() => {
    let mocks = createMockApplicationStore();
    appStoreMock = mocks.appStoreMock;
    githubMock = mocks.githubMock;
    project = {
      id: 123,
      name: "Proj1",
      description: "Description 1",
      avatarUrl: "http://gravatar1",
      htmlUrl: "http://url",
      forks: 20,
      issues: 30,
      watchers: 10,
      language: "Javascript"
    };
  })

  test('displays the name of the project', ()=> {
    let { getByText } = render(<Project store={appStoreMock} project={project} />);
    expect(getByText('Proj1')).toBeInTheDocument();
  })

  test('displays number of forks', ()=> {
    let { getByText } = render(<Project store={appStoreMock} project={project} />);
    expect(getByText('20')).toBeInTheDocument();
  })

  test('triggers loading commit when project link clicked', () => {
    let { getByRole } = render(<Project store={appStoreMock} project={project} />);
    fireEvent.click(getByRole('project-view'));
    expect(githubMock.loadPagedCommits).toHaveBeenCalled();
  })

});