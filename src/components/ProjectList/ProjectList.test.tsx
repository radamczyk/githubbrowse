import React from 'react';
import { render, act, screen} from '@testing-library/react';
import ProjectList from './ProjectList';
import createMockApplicationStore from '../testhelpers/mockappstore';
import { IApplicationStore } from '../../stores/IApplicationStore';
import IGithubClient from '../../github/IGithubClient';

describe('Project component', () => {

  let appStoreMock: IApplicationStore,
      githubMock: IGithubClient;

  beforeEach(() => {
    let mocks = createMockApplicationStore();
    appStoreMock = mocks.appStoreMock;
    githubMock = mocks.githubMock;
  })

  test('displays organization name', async () => {
    appStoreMock.loadGithubProjects('myorg');
    appStoreMock.organization = 'myorg';
    await act(async () => {
      render(<ProjectList store={appStoreMock}/>);
    })
    expect(screen.getByText('myorg')).toBeInTheDocument();
  })

  test('displays all projects', async () => {
    githubMock.loadAllProjects = jest.fn().mockReturnValue(Promise.resolve(
      [{
        id: 1,
        name: "Proj1",
        description: "Description 1",
        avatarUrl: "http://gravatar1",
        htmlUrl: "http://url",
        forks: 20,
        issues: 30,
        watchers: 10,
        language: "Javascript"
      },
      {
        id: 2,
        name: "Proj2",
        description: "Description 2",
        avatarUrl: "http://gravatar1",
        htmlUrl: "http://url",
        forks: 10,
        issues: 30,
        watchers: 10,
        language: "Javascript"
      }]
    ));
    await appStoreMock.loadGithubProjects('myorg');
    let { getByText } = render(<ProjectList store={appStoreMock}/>);
    expect(getByText('Proj1')).toBeInTheDocument();
    expect(getByText('Proj2')).toBeInTheDocument();
  })

});