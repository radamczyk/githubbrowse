import React from 'react';
import IGithubProject from '../../github/IGithubProject';
import { ListGroup } from 'react-bootstrap';
import { IApplicationStore } from '../../stores/IApplicationStore';
import './Project.css';

type ProjectProps = {
  project: IGithubProject,
  store: IApplicationStore
}

export default function Project(props: ProjectProps) {
  const project: IGithubProject = props.project;
  const store: IApplicationStore = props.store;

  function loadCommits(projectName: string) {
    store.loadCommits(projectName);
  }

  return (
    <ListGroup.Item as="li" className="Project">
        <a className="Project-name" href="#" role="project-view" onClick={() => loadCommits(project.name)}>{project.name}</a>
        <span className="Project-forks">forks: <b>{project.forks}</b></span>
    </ListGroup.Item>
  )
}

