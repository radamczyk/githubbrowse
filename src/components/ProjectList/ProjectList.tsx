import React from 'react';
import { observer } from 'mobx-react';
import { ListGroup, Col } from 'react-bootstrap';
import Project from './Project';
import { IApplicationStore, IProjectStore } from '../../stores/IApplicationStore';
import Paginate from '../Paginate/Paginate';
import './ProjectList.css';

type ProjectListProps = {
  store: IApplicationStore
}

function ProjectList(props: ProjectListProps) {
  const projectStore: IProjectStore = props.store.projectStore;
  const orderedProjects = projectStore.orderedProjects;
  const avatarUrl = orderedProjects.length ? orderedProjects[0].avatarUrl : "";
  const projects = orderedProjects.map((project) =>
    <Project project={project} store={props.store} key={project.id}/>
  );

  return (
    <Col xs="3" className="ProjectList AppList">
      <header>
        <div className="ProjectList-orgheader">
          <img src={avatarUrl} height="50px" alt=""/>
          <span className="ProjectList-name">{projectStore.organization}</span>
        </div>
        <h4>Repositories</h4>
      </header>

      <Paginate paginatedStore={projectStore}/>
      <ListGroup as="ul">
        {projects}
      </ListGroup>
    </Col>
  )
}

export default observer(ProjectList);
