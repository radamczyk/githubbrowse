import IGithubClient from "../../github/IGithubClient";
import createStore from '../../stores/ApplicationStore';
import IGithubProject from "../../github/IGithubProject";
import IGithubPagedCommits from "../../github/IGithubPagedCommits";
import { IApplicationStore } from "../../stores/IApplicationStore";
import IGithubCommit from "../../github/IGithubCommit";

/**
 * Function create base github and application store mocks
 * which are used to test react components.
 */
export default function createMockApplicationStore() {
  let githubMock: IGithubClient;
  let projectsMockData: IGithubProject[];
  let commitsMockData: IGithubCommit[];
  let appStore: IApplicationStore;

  projectsMockData = [{
    id: 123,
    name: "Proj1",
    description: "Description 1",
    avatarUrl: "http://gravatar1",
    htmlUrl: "http://url",
    forks: 20,
    issues: 30,
    watchers: 10,
    language: "Javascript"
  }];

  commitsMockData = [{
    author: {
      name: "John Doe",
      avatarUrl: "http://gravatar/user"
    },
    date: new Date(),
    url: "http://github",
    message: "fix:bugs",
    sha: "123abdc"
  }];

  githubMock = {
    loadAllProjects: jest.fn().mockReturnValue(Promise.resolve(projectsMockData)),
    loadPagedCommits: jest.fn().mockReturnValue(Promise.resolve({
      commits: commitsMockData,
      page: 1,
      perPage: 10,
      totalPages: 100
    }))
  };

  appStore = createStore(githubMock);
  return {
    appStoreMock: appStore,
    githubMock: githubMock
  }
}