import React from 'react';
import { render } from '@testing-library/react';
import ErrorAlert from './ErrorAlert';
import createMockApplicationStore from '../testhelpers/mockappstore';
import { IApplicationStore } from '../../stores/IApplicationStore';
import IGithubClient from '../../github/IGithubClient';
import { GithubErrorType } from '../../github/IGithubError';

describe('ErrorAlert component', () => {
  let appStoreMock: IApplicationStore,
      githubMock: IGithubClient,
      container: HTMLElement;

  beforeEach(() => {
    let mocks = createMockApplicationStore();
    appStoreMock = mocks.appStoreMock;
    githubMock = mocks.githubMock;
  })

  test('shows org not found message when error is triggered', () => {
    appStoreMock.organization = 'myorganization';
    appStoreMock.error = {
      type: GithubErrorType.ORGANIZATION_DOES_NOT_EXIST
    };
    const { getByText } = render(<ErrorAlert store={appStoreMock}/>);
    expect(getByText(/We could not find organization.*/)).toBeInTheDocument();
  })

  test('show regular error for unspecified errors', () => {
    appStoreMock.organization = 'myorganization';
    appStoreMock.error = {
      type: GithubErrorType.UNSPECIFIED_ERROR
    };
    const { getByText } = render(<ErrorAlert store={appStoreMock}/>);
    expect(getByText(/We could not load data from.*/)).toBeInTheDocument();
  });

});