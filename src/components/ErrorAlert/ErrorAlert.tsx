import React from 'react';
import { observer } from 'mobx-react';
import { IApplicationStore } from '../../stores/IApplicationStore';
import { Alert } from 'react-bootstrap';
import { GithubErrorType } from '../../github/IGithubError';

type ErrorAlertProps = {
  store: IApplicationStore
}

function ErrorAlert(props:ErrorAlertProps) {
  const store = props.store;
  const error = store.error;

  if (error) {
    let errorMessage

    switch(error.type) {
      case GithubErrorType.ORGANIZATION_DOES_NOT_EXIST:
        errorMessage = (<>We could not find organization
          <b> {store.organization}</b>. Please type in new github organization name and hit enter.</>);
        break;
      case GithubErrorType.UNSPECIFIED_ERROR:
        errorMessage = (<>We could not load data from <b>github.com</b>. Please try again later.</>);
        break
    };

    return (
      <Alert variant="danger" className="ErrorAlert" onClose={() => store.clearStatus()} dismissible>
        <Alert.Heading>Houston, we got a Problem!</Alert.Heading>
        <p>
          {errorMessage}
        </p>
      </Alert>
    )
  }

  return (
    <>
    </>
  )
}

export default observer(ErrorAlert);
