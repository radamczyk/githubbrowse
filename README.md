## Installation

This project requires. NodeJS installed. NodeJS can be downloaded from [here](https://nodejs.org/en/download/).
It also requires `yarn` package manager to be installed.
After installing NodeJs please run:

`npm install -g yarn`

After yarn in installed please run:

`yarn`

in the project main directory to install all dependencies.

to avoid being capped at 60 request per hour please add your *github auth token* in config file.

`./src/config.json`

it is also possible to setup api endpoint in the config file.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test`

Launches the test runner in the interactive watch mode.

### `yarn build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.

## About the application
Application loads projects for organization provided by th user. User needs to input organization name into the search input and wait until projects get loaded. After repositories are loaded it is possible to select one project from the list and display all commits for this project and browse through them.

The projects are sorted by using number of forks with the highest on the top of the list.

Unfortunately it looks like Github API does not support adding number of forks as a parameter for sorting, hence after providing github organization application is first loading all of the open source projects and does the sorting on the client side. However it can take a while to load projects for organizations which have thousands of projects as github api caps the max results at 100 per page and the application needs to load all of the pages before sorting them and displaying to the user.

The commits are loaded when project is selected at rate of 30 commits per page. Paging component allows navigating nad
loading newer and older commits.

### Tooling choices

#### React
As choice for the UI framework. React is used quite extensively hence i took this opportunity to ramp up and refresh my react knowledge..  Plus react app scaffolding creates quite a nice default application setup which saves time figuring up webpack setup etc. Other options could be Angular, Vue, or native browser webcompoenents (as they are supported nowdays in all browsers), however React seemed the most robust.

#### Typescript
Using Typescript can heavily reduce bugs related to npe-es and missing attributes due to adding types, plus it allows
defining interfaces which then work nicely to abstract implementation details for certain parts of the app. However
it can be double edged sword as well especially as some of the libraries and tools do not have type libs defines or might not work with it and can cause spending some time to figure out the glitches.

#### MobX
For app state containers it seemed like a good alternative to redux with much lower effort time to set up.

#### Ocktokit
To request github api. It has some nice build functionality like 'paginate' and load all the pages which was
needed to load all of the projects before sorting them.

#### React bootstrap
For basic look & feel components.

### App architecture choices
* Use react just as a thin UI component layer, only responsible for rendering and passing actions to application core stores and reacting to store data changes via observers (no business logic in UI).
* Use mobx to create observable stores to provide app actions and manage data (this is where the application actions logic is placed)
* Github (data) access wrapped and exposed to stores via interfaces

Having these 3 layers segregated and utilizing interfaces creates possibility to improve one of the layer without modifying another i.e with low level of effort we can change GithubClient implementation to fetch data from bitbucket without
changing stores or UI layer (as long as it will implement IGithubInterface, and domain model interfaces IGithubCommit etc...).

## Future Improvements
If i had more time there are potential things which I could still improve

* Cleanup imports
* More configuration in config i.e 'perPage' etc, right now it is arbitrary in the code.
* Perhaps better naming i.e IGitInterface instead of IGithubInterface (to make it more generic).
* Figuring out react providers to 'inject' stores to components instead of passing through properties.
* More UI touches, i.e adding additional spinners to clear-er indicate loading for certain parts of the page
* Adding more information to pagination
* Even more UI touches i.e error message self fade-out animations etc.
* Better error handling
* Logging and monitoring (if app was to run in prod)

However it will require much more time to invest and the list might be bigger. Nevertheless I think the current state of the application is a functional MVP users can use to browse commits.